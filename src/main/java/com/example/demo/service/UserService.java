package com.example.demo.service;

import com.example.demo.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final OrderService orderService;

    @Autowired
    public UserService(OrderService orderService) {
        this.orderService = orderService;
    }

    void updateUser(int userId, UserDto userDto) {
        orderService.addOrderToUser(userDto);
    }
}
