package com.example.demo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class RezeptEntity {
    @Id
    @GeneratedValue
    Integer rezeptId;

    @ManyToOne()
    ArztEntity arzt;

    // arztId wird nicht benötigt, da JPA das automatisch durch @ManyToOne bei arzt macht
    // Integer arztId;

    @ManyToOne
    ArztEntity oberarzt;
}
