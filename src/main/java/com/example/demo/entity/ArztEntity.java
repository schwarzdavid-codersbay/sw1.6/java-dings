package com.example.demo.entity;

import jakarta.persistence.*;

import java.util.List;

@Entity
public class ArztEntity {
    @Id
    @GeneratedValue
    Integer arztId;

    @Column
    String position;

    @OneToMany(mappedBy = "arzt")
    List<RezeptEntity> rezepte;

    @OneToMany(mappedBy = "oberarzt")
    List<RezeptEntity> freigegebeneRezepte;
}
