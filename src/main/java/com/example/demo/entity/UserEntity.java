package com.example.demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer userId;

    @Column
    String origin;

    @Column
    String firstName;

    @Column
    String email;
}
