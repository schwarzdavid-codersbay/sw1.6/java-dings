package com.example.demo.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public class UserCreateDto {

    @NotNull
    @NotEmpty
    private String firstName;
    private String lastName;
    private Integer age;

    public UserCreateDto(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserCreateDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public UserCreateDto setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserCreateDto setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }
}
