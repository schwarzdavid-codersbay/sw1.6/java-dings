package com.example.demo.controller;

import com.example.demo.dto.UserDto;
import com.example.demo.exception.ConflictFieldException;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLIntegrityConstraintViolationException;

@RestController()
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    String doSomething() {
        UserDto userDto = doSomethingElse();
        return "Hallo Welt";
    }

    UserDto doSomethingElse() {
        UserDto user = null;
        user.setFirstName("Max");
        user.setLastName("Muster");
        if(true) {
            ConflictFieldException conflictFieldException = new ConflictFieldException();
            conflictFieldException.addConflictingField("email");
            throw conflictFieldException;
        }
        return user;
    }

    @GetMapping
    ResponseEntity getRandomString() {
        try {
            return ResponseEntity.ok(doSomething());
        } catch (NullPointerException err) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
        } catch (SQLIntegrityConstraintViolationException err) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("validation.email.constraint");
        }
    }
}
