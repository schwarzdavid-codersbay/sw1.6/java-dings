package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ConflictFieldException extends ResponseStatusException {
    List<String> conflictingFields = new ArrayList<>();

    public ConflictFieldException() {
        super(HttpStatus.CONFLICT);
    }

    public void addConflictingField(String fieldName) {
        conflictingFields.add(fieldName);
    }

    @Override
    public String toString() {
        /*String output = "";
        for (String field : conflictingFields) {
            output += "validation." + field + ".constraint\n";
        }
        return output;*/
        return conflictingFields.stream()
                .map(field -> "validation." + field + ".constraint")
                .collect(Collectors.joining("\n"));
    }
}
